import mongoose from "mongoose";
const connection = {}
async function connectDb() {
    console.log("connecting db...")

    if(connection.isConnected){
        console.log("Connected")
        return
    }
    if(mongoose.connection.length > 0){
        connection.isConnected = mongoose.connection[0].readyState
        if(connection.isConnected===1){
            crossOriginIsolated.log("Use previous con to the database")
            return
        }
        await mongoose.disconnect()

    }
    const db=await mongoose.connect(process.env.MONGODB_URL,{
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).catch(error => console.log(error));
    console.log("New connection to the database.");
    connection.isConnected = db.connections[0].readyState;

}
async function disconnectDb(){
    if (connection.isConnected) {
        if (process.env.NODE_END === "production") {
          await mongoose.disconnect();
          connection.isConnected = false;
        } else {
          console.log("not disconnecting from the database.");
        }
    }
}

const db = { connectDb, disconnectDb };
export default db;