export const offersAarray = [
  {
    image:
      "./images/offers/book_detail_large.gif",
    price: "150",
    discount: "17",
  },
  {
    image:
      "./images/offers/book_detail_large (0).gif",
    price: "150",
    discount: "8",
  },
  {
    image:
      "./images/offers/book_detail_large (1).gif",
    price: "150",
    discount: "69",
  },
  {
    image:
      "./images/offers/book_detail_large (2).gif",
    price: "150",
    discount: "32",
  },
  {
    image:
      "./images/offers/book_detail_large (3).gif",
    price: "150",
    discount: "90",
  },
  {
    image:
      "./images/offers/book_detail_large (4).gif",
    price: "150",
    discount: "45",
  },
  {
    image:
      "./images/offers/book_detail_large (5).gif",
    price: "150",
    discount: "10",
  },
  {
    image:
      "./images/offers/book_detail_large (6).gif",
    price: "150",
    discount: "5",
  },
  {
    image:
      "./images/offers/book_detail_large (7).gif",
    price: "150",
    discount: "5",
  },
  {
    image:
      "./images/offers/book_detail_large (8).gif",
    price: "150",
    discount: "5",
  },
];
export const flashDealsArray = [
  {
    image:
      "./images/flashsales/web_large2 (1).gif",
    price: "91",
    discount: "11",
    link: "",
    sold: "30",
  },
  {
    image:
      "./images/flashsales/web_large2 (2).gif",
    price: "73",
    discount: "25",
    link: "",
    sold: "61",
  },
  {
    image:
      "./images/flashsales/web_large2 (3).gif",
    price: "254",
    discount: "36",
    link: "",
    sold: "12",
  },
  {
    image:
      "./images/flashsales/web_large2 (4).gif",
    price: "1687",
    discount: "20",
    link: "",
    sold: "96",
  },
  {
    image:
      "./images/flashsales/web_large2 (5).gif",
    price: "126",
    discount: "8",
    link: "",
    sold: "12",
  },
  {
    image:
      "./images/flashsales/web_large2 (6).gif",
    price: "65",
    discount: "7",
    link: "",
    sold: "82",
  },
  {
    image:
      "./images/flashsales/web_large2 (7).gif",
    price: "41",
    discount: "9",
    link: "",
    sold: "52",
  },
  {
    image:
      "./images/flashsales/web_large2.gif",
    price: "288",
    discount: "35",
    link: "",
    sold: "49",
  },
];
export const book01 = [
  {
    image:
      "./images/product/book01/web_large2 (1).gif",
    price: "79.58",
  },
  {
    image:
      "./images/product/book01/web_large2 (2).gif",
    price: "",
  },
  {
    image:
      "./images/product/book01/web_large2 (3).gif",
    price: "",
  },
  {
    image:
      "./images/product/book01/web_large2 (4).gif",
    price: "",
  },
  {
    image:
      "./images/product/book01/web_large2 (8).gif",
    price: "",
  },
  {
    image:
      "./images/product/book01/web_large2.gif",
    price: "",
  },
];
export const book02 = [
  {
    image:
      "./images/product/book02/web_large2 (1).gif",
  },
  {
    image:
      "./images/product/book02/web_large2 (2).gif",
  },
  {
    image:
      "./images/product/book02/web_large2 (3).gif",
  },
  {
    image:
      "./images/product/book02/web_large2 (4).gif",
  },
  {
    image:
      "./images/product/book02/web_large2 (5).gif",
  },
  {
    image:
      "./images/product/book02/web_large2.gif",
  },
];
export const book03 = [
  {
    image:
      "./images/product/book03/web_large2 (1).gif",
  },
  {
    image:
      "./images/product/book03/web_large2 (2).gif",
  },
  {
    image:
      "./images/product/book03/web_large2 (3).gif",
  },
  {
    image:
      "./images/product/book03/web_large2 (4).gif",
  },
  {
    image:
      "./images/product/book03/web_large2 (6).gif",
  },
  {
    image:
      "./images/product/book03/web_large2.gif",
  },
];
export const book_swiper_01 = [
  {
    name: "HARRY POTTER | SHEIN Checkered & Snake Pattern Sweater Vest Without Blouse",
    image:
      "./images/product/book_other/web_large2 (1).gif",
    price: "103",
  },
  {
    name: "Blazer manches cape à bouton",
    image:
      "./images/product/book_other/web_large2 (2).gif",
    price: "58",
  },
  {
    name: "SHEIN X Zhihan Wang Sweat-shirt dessin animé et slogan",
    image:
      "./images/product/book_other/web_large2 (3).gif",
    price: "12.99",
  },
  {
    name: "ROMWE Grunge Punk Sweat-shirt à capuche à imprimé lettre et graphique à cordon",
    image:
      "./images/product/book_other/web_large2 (4).gif",
    price: "396",
  },
  {
    name: "15pcs Gemstone & Rhinestone Decor Ring",
    image:
      "./images/product/book_other/web_large2 (5).gif",
    price: "5.31",
  },
  {
    name: "5pcs Halloween Pumpkin & Ghost Design Shoe Decoration",
    image:
      "./images/product/book_other/web_large2 (6).gif",
    price: "79.25",
  },
  {
    name: "10pcs Solid Hair Band",
    image:
      "./images/product/book_other/web_large2 (7).gif",
    price: "3.69",
  },
  {
    name: "Rhinestone Decor Bracelet With Ring",
    image:
      "./images/product/book_other/web_large2 (8).gif",
    price: "12.5",
  },
  {
    name: "Colorful Leaf Print Pocket Front Backpack",
    image:
      "./images/product/book_other/web_large2 (9).gif",
    price: "11.58",
  },
];