import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.scss'
import Header from '../components/header'
import Footer from '../components/footer'
import Menu from '../components/home/menu'
import Main from '../components/home/main'
import FlashDeals from '@/components/home/flashDeals'
import axios from 'axios'
import { useSession, signIn, signOut } from "next-auth/react"
import Category from "../components/home/category";
import { useMediaQuery } from "react-responsive";
import ProductsSwiper from "../components/productsSwiper";

import {
  book_swiper_01,
  book01,
  book02,
  book03,
} from "../data/home";

export default function Home({ country }) {
  const { data: session } = useSession()
  const isMedium = useMediaQuery({ query: "(max-width:850px)" });
  const isMobile = useMediaQuery({ query: "(max-width:550px)" });

  // console.log(session)
  return (
    <div>
      <Header />
      <div className={styles.home}>
        <div className={styles.container}>
          <Menu />
          <Main />
          <FlashDeals />
          <div className={styles.home__category}>
            <Category
              header="Award Winning"
              products={book01}
              background='#FFFFFF'
            />
            {!isMedium && (
              <Category
                header="Best Selling"
                products={book02}
                background='#FFFFFF'
              />
            )}
            {isMobile && (
              <Category
                header="Best Selling"
                products={book02}
                background='#FFFFFF'
              />
            )}
            <Category
              header="Filmmaking"
              products={book03}
              background='#FFFFFF'
            />
          </div>
          <ProductsSwiper products={book_swiper_01} header='Category Romance' bg='#1895f5' />
          <ProductsSwiper products={book_swiper_01} header='Category Fantasy' bg='#1895f5' />
          <ProductsSwiper products={book_swiper_01} header='Category Horror' bg='#1895f5' />
          <ProductsSwiper products={book_swiper_01} header='Bestseller' bg='#1895f5' />

        </div>
      </div>
      <Footer country={country} />
    </div>
  )
}

export async function getServerSideProps() {
  let data = await axios
    .get('https://api.ipregistry.co/?key=007pia8elcb7460l')
    .then((res) => {
      return res.data.location.country;
    })
    .catch((err) => {
      console.log(err);
    });
  return {
    props: {
      // country:{name:data.name},
      country: { name: 'Thailand' },


    }
  }

}