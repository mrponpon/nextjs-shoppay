import Header from "@/components/header";
import Footer from "@/components/footer";
import styles from "../styles/signin.module.scss"
import { BiLeftArrowAlt } from "react-icons/bi"
import { Formik, Form } from "formik";
import Link from "next/link";
import { useState } from "react";
import LoginInput from "@/components/inputs/logininput";
import * as Yup from "yup"
import CircledIconBtn from "@/components/buttons/circledIconBtn";
import {
    getCsrfToken,
    getProviders,
    getSession,
    signIn,
    country,
} from "next-auth/react";
const initialvaluse = {
    login_email: "",
    login_password: "",
};
function signin({ providers }) {
    const [user, setUser] = useState(initialvaluse);
    const { login_email, login_password } = user;
    const handleChange = (e) => {
        const { name, value } = e.target;
        setUser({ ...user, [name]: value });
    };
    console.log('user_loginput', user)
    const loginValidation = Yup.object({
        login_email: Yup.string()
            .required("Email is required.")
            .email("Please enter a valid email."),
        login_password: Yup.string()
            .required()
            .email("Please enter a Password."),

    });
    return (
        <>
            <Header />
            <div className={styles.login}>
                <div className={styles.login__container}>
                    <div className={styles.login__header}>
                        <div className={styles.back__svg}>
                            <BiLeftArrowAlt />
                        </div>
                        <span>
                            Back to store
                        </span>
                    </div>
                    <div className={styles.login__form}>
                        <h1>Sign in</h1>
                        <p>
                            Get access
                        </p>
                        <Formik
                            enableReinitialize
                            initialValues={
                                {
                                    login_email,
                                    login_password,
                                }
                            }
                            validationSchema={loginValidation}
                        >
                            {
                                (form) => (
                                    <Form>
                                        <LoginInput
                                            type="text"
                                            name="login_email"
                                            icon='user'
                                            placeholder='Email'
                                            onChange={handleChange}
                                        />
                                        <LoginInput
                                            type="password"
                                            name="login_password"
                                            icon='password'
                                            placeholder='Password'
                                            onChange={handleChange}
                                        />
                                        <CircledIconBtn type='submit' text='Sign in' />
                                        <div className={styles.forgot}>
                                            <Link href='/forget'>Forgot password</Link>
                                        </div>
                                    </Form>
                                )
                            }
                        </Formik>
                        <div className={styles.login__socials}>
                            <span className={styles.or}>Or continue with</span>
                            <div className={styles.login__socials_wrap}>
                                {providers.map((provider) => {
                                    if (provider.name == "Credentials") {
                                        return;
                                    }
                                    return (
                                        <div key={provider.name}>
                                            <button
                                                className={styles.social__btn}
                                                onClick={() => signIn(provider.id)}
                                            >
                                                <img src={`../../icons/${provider.name}.png`} alt="" />
                                                Sign in with {provider.name}
                                            </button>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer country='Thailand' />
        </>
    );
}

export default signin;
export async function getServerSideProps(context) {
    const providers = Object.values(await getProviders());
    return {
        props: { providers },
    }
}