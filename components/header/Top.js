import React from 'react';
import styles from "./styles.module.scss";
import { MdSecurity } from "react-icons/md";
import { BsSuitHeart } from "react-icons/bs";
import { AiOutlineQuestionCircle } from "react-icons/ai";
import { RiAccountPinCircleLine, RiArrowDropDownFill } from "react-icons/ri";
import Link from "next/link";
import { useState } from "react";
import UserMenu from './UserMenu';
import { useSession } from 'next-auth/react';
function Top() {
    const { data: session } = useSession()
    const [visible, setVisible] = useState(false);
    console.log("Top_component_session", session)

    return (
        <div className={styles.top}>
            <div className={styles.top__container}>
                <div>
                </div>
                <ul className={styles.top__list}>
                    <li className={styles.li} style={{ color: 'white' }}>
                        <MdSecurity />
                        <span>
                            Buyer Protection
                        </span>
                    </li>
                    <li className={styles.li} style={{ color: 'white' }}>
                        <AiOutlineQuestionCircle />
                        <span>
                            Customer Service
                        </span>
                    </li>
                    <li className={styles.li} style={{ color: 'white' }}>
                        <BsSuitHeart />
                        <Link href="/profile/whishlist">
                            <span>Whishlist</span>
                        </Link>
                    </li>
                    <li className={styles.li}
                        onMouseOver={() => setVisible(true)}
                        onMouseLeave={() => setVisible(false)}
                    >
                        {session ? (
                            <li>
                                <div className={styles.flex}>
                                    <img
                                        src={session.user.image}
                                        alt=""
                                        className={styles.img}
                                    />
                                    <span>{session.user.name}</span>
                                    <RiArrowDropDownFill />
                                </div>
                            </li>) : (
                            <li className={styles.li} style={{ color: 'white' }}>
                                <div className={styles.flex}>
                                    <RiAccountPinCircleLine />
                                    <span>Account</span>
                                    <RiArrowDropDownFill />
                                </div>
                            </li>)}

                        {visible && <UserMenu session={session} />}
                    </li>
                </ul>
            </div>
        </div>
    );
}

export default Top;