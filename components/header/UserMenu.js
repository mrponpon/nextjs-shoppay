import styles from "./styles.module.scss";
import Link from "next/link";
import { signOut, signIn } from 'next-auth/react';

function UserMenu({ session }) {
    return (
        <div className={styles.menu}>
            {session ? (
                <div className={styles.flex}>
                    <img
                        src={session.user.image}
                        alt=""
                        className={styles.menu__img}
                    />
                    <div className={styles.col}>
                        <span>Welcome Back</span>
                        <h3>{session.user.name}</h3>
                    </div>
                </div>

            ) : (
                <div>
                    <h4>Welcome</h4>
                    <div className={styles.flex}>
                        <button className={styles.btn_primary}>Register</button>

                    </div>
                    <div className={styles.flex}>
                        <button className={styles.btn_outlined} onClick={() => signIn()}>Login</button>
                    </div>
                </div>
            )}
            <hr className={styles.hr}></hr>
            <ul>
                <li>
                    <Link href="/profile">Account</Link>
                </li>
                <li>
                    <Link href="/profile/orders">My Orders</Link>
                </li>
                <li>
                    <Link href="/profile/messages">Message Center</Link>
                </li>
                <li>
                    <Link href="/profile/address">Address</Link>
                </li>
                <li>
                    <Link href="/profile/whishlist">Whishlist</Link>
                </li>
            </ul>
            {session ? (
                <>
                    <hr className={styles.hr}></hr>
                    <ul>
                        <li>
                            <a onClick={() => signOut()}>Sign Out</a>
                        </li>
                    </ul>
                </>
            ) : (
                null
            )}
        </div>
    );
}

export default UserMenu;