import styles from './styles.module.scss';

function Payment(props) {
    return (
        <div className={styles.footer__payment}>
            <h3>WE ACCEPT</h3>
            <div className={styles.footer__flexwrap}>
                <img src="images/payment/visa.webp" alt=""></img>
                <img src="images/payment/mastercard.webp" alt=""></img>
                <img src="images/payment/paypal.webp" alt=""></img>

            </div>
            
        </div>
    );
}

export default Payment;