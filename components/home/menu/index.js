import styles from "./styles.module.scss";

function Menu() {
    return (
        <div className={styles.menu}>
            <div className={styles.menu__container}>
                <ul className={styles.menu__list}>
                    <li className={styles.li} style={{ color: '#666' }}>
                        <span>
                            Home
                        </span>
                    </li>
                    <li className={styles.li} style={{ color: '#666' }}>
                        <span>
                            Best seller
                        </span>
                    </li>
                    <li className={styles.li} style={{ color: '#666' }}>
                        <span>
                            New
                        </span>
                    </li>
                    <li className={styles.li} style={{ color: '#666' }}>
                        <span>
                            Promotion
                        </span>
                    </li>
                    <li className={styles.li} style={{ color: '#666' }}>
                        <span>
                            Free
                        </span>
                    </li>
                    <li className={styles.li} style={{ color: '#666' }}>
                        <span>
                            Recommended
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    );
}

export default Menu;