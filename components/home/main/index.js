import styles from './styles.module.scss';
import Offers from './offers';
import MainSwiper from './swiper';
function Main() {
    return (
        <div className={styles.main}>
            <div />
            <MainSwiper />
            <Offers />
        </div>
    )
}

export default Main;